from setuptools import setup

setup(
    name='billcompare',
    version='0.8',
    packages=['billcompare'],
    install_requires=[
          'pandas', 'xlrd', 'PyQt5', 'openpyxl'
      ],
    package_dir={'': 'billcompare'},
    url='',
    license='',
    author='Constantine Vinogradov',
    author_email='k.a.vinogradov@gmail.com',
    description='Compares CMD bill with data from AL LIMS'
)
