# -*- coding: utf-8 -*-
from billcompare.mainwindow import *
from billcompare.engine import input_path_to_output_path, compare
from PyQt5 import QtWidgets
import xlrd
import os.path


class MWin(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        super(MWin, self).__init__(parent=parent)

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.cmdOpen.clicked.connect(self.openFileNameDialogCMD)
        self.ui.lqOpen.clicked.connect(self.openFileNameDialogLQ)
        self.ui.resultSave.clicked.connect(self.saveFileDialog)
        self.ui.compareButton.clicked.connect(self.compare_bills)
        self.ui.closeButton.clicked.connect(self.close)




    def openFileNameDialogCMD(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Открыть файл", "",
                                                            "Excel File (*.xls, *.xlsx);;All Files (*)")
        if fileName:
            self.ui.cmdFilePath.setText(fileName)
            self.ui.resultFilePath.setText(input_path_to_output_path(fileName))

    def openFileNameDialogLQ(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Открыть файл", "",
                                                            "CSV File (*.csv);;All Files (*)")
        if fileName:
            self.ui.lqFilePath.setText(fileName)


    def saveFileDialog(self):
        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Сохранить как", "",
                                                            "Excel File (*.xlsx, *.xls);;All Files (*)")
        if fileName:
            file_name_with_extens = '{}.xlsx'.format(fileName)
            self.ui.resultFilePath.setText(file_name_with_extens)

    def compare_bills(self):
        cmd_bill = self.ui.cmdFilePath.text()
        lq_bill = self.ui.lqFilePath.text()
        compare_result = self.ui.resultFilePath.text()
        if not os.path.isfile(cmd_bill) or not os.path.isfile(lq_bill):
            self.okDialog(message_text='Указанный исходный файл не существует')
            return
        elif os.path.isfile(compare_result):
            self.okDialog(message_text='Выходной файл уже существует')
            return
        else:
            try:
                compare(cmd_input_excel_file=cmd_bill, lq_input_csv_file=lq_bill, compare_result_file=compare_result)
                self.okDialog(window_title='Успех', message_text='Сравнение завершено')
            except xlrd.biffh.XLRDError:
                self.okDialog(message_text='Некорректный формат файла CMD')




    def okDialog(self, window_title='Ошибка', message_text=''):
        d = One_button_dialog()
        d.setWindowTitle(window_title)
        d.messageText.setText(message_text)
        d.exec_()
