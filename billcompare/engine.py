# -*- coding: utf-8 -*-
import pandas as pd
import csv
import tempfile
import datetime
import os


def input_path_to_output_path(file_path):
    reversed_string = file_path[::-1]
    dot_index = reversed_string.index('.') + 1
    without_extension = (reversed_string[dot_index::])[::-1]
    dte = str(datetime.datetime.now())[:10]
    output_path = f'{without_extension}_сверка_{dte}.xlsx'
    return output_path


def csv_to_list(csv_file, encoding):
    list_to_return = []
    with open(csv_file, encoding=encoding, newline='') as csvfile:
        string_reader = csv.reader(csvfile, delimiter=';')
        for row in string_reader:
            if len(row[0]) > 0:
                list_to_return.append(row)
    return list_to_return


def list_to_csv(input_list, output_csv):
    with open(output_csv, 'w', newline='', encoding='utf-8') as r:
        writer = csv.writer(r, delimiter=';', quotechar='|', quoting=csv.QUOTE_ALL)
        writer.writerows(input_list)


def cmd_bill_convert(cmd_list):
    output_list = []
    for row in cmd_list:
        internal_nr = row[0]
        if internal_nr[0] == "'":
            internal_nr = internal_nr[1:]
        if '.' in internal_nr:
            internal_nr = internal_nr[:internal_nr.index('.')]
        fio_full = row[1].upper()
        fi = fio_full[:fio_full.find(' ') + 2]
        tmp_list = row[2].split(',')
        for service_no, service in enumerate(tmp_list):
            service_code = service[2:8]
            if len(service_code) == 6:
                try:
                    int(service_code)
                    internal_nr_service = internal_nr + '-' + service_code
                    fi_service = fi + '-' + service_code
                    temp_list = [str(internal_nr), fio_full, str(service_code), internal_nr_service, fi_service, fi]
                    output_list.append(temp_list)
                except ValueError:
                    pass
    return output_list


def lq_bill_convert(lq_list):
    output_list = []
    for row in lq_list:
        internal_nr = row[0]
        fio_full = row[1].upper()
        fi = fio_full[:fio_full.find(' ') + 2]
        service = row[2]
        internal_nr_service = internal_nr + '-' + service
        fi_service = fi + '-' + service
        tmp_list = [internal_nr_service, fi_service, internal_nr, fio_full, service]
        output_list.append(tmp_list)
    return output_list


def bill_compare(cmd_list, lq_list):
    output_list = []
    lq_set = []
    lq_set_names = []
    cmd_set = []
    cmd_set_names = []
    cmd_list_2 = []
    for row in lq_list:
        nr_serv_code_lq = row[0]
        lq_set.append(nr_serv_code_lq)
    for row in cmd_list:
        nr_serv_code_cmd = row[3]
        cmd_set.append(nr_serv_code_cmd)
    lq_set = set(lq_set)
    cmd_set = set(cmd_set)
    cmd_set.difference_update(lq_set)
    for row in cmd_list:
        nr_serv_code_cmd = row[3]
        for nr_serv_code in cmd_set:
            if nr_serv_code == nr_serv_code_cmd:
                cmd_list_2.append(row)
    for row in lq_list:
        fi_serv_lq = row[1]
        lq_set_names.append(fi_serv_lq)
    for row in cmd_list_2:
        fi_serv_cmd = row[4]
        cmd_set_names.append(fi_serv_cmd)
    lq_set_names = set(lq_set_names)
    cmd_set_names = set(cmd_set_names)
    cmd_set_names.difference_update(lq_set_names)
    for row in cmd_list_2:
        fi_serv_cmd = row[4]
        for fi_serv in cmd_set_names:
            if fi_serv == fi_serv_cmd:
                output_list.append(row)
    return output_list


def lq_remaining(cmd_list, lq_list):
    lq_set = []
    lq_set_names = []
    cmd_set = []
    cmd_set_names = []
    lq_list_2 = []
    lq_set_fio = []
    lq_set_by_names = []
    for row in lq_list:
        nr_serv_code_lq = row[0]
        lq_set.append(nr_serv_code_lq)
    for row in cmd_list:
        nr_serv_code_cmd = row[3]
        cmd_set.append(nr_serv_code_cmd)
    lq_set = set(lq_set)
    cmd_set = set(cmd_set)
    lq_set.intersection_update(cmd_set)
    for row in lq_list:
        nr_serv_code_cmd = row[0]
        for nr_serv_code in lq_set:
            if nr_serv_code == nr_serv_code_cmd:
                lq_list_2.append(tuple(row))
    for row in lq_list:
        fi_serv_lq = row[1]
        lq_set_names.append(fi_serv_lq)
    for row in cmd_list:
        fi_serv_cmd = row[4]
        cmd_set_names.append(fi_serv_cmd)
    lq_set_names = set(lq_set_names)
    cmd_set_names = set(cmd_set_names)
    lq_set_names.intersection_update(cmd_set_names)
    for row in lq_list:
        fi_serv_lq = row[1]
        for fi_serv in lq_set_names:
            if fi_serv == fi_serv_lq:
                lq_list_2.append(tuple(row))
    for row in lq_list_2:
        lq_set_fio.append(row[3])
    lq_set_fio = set(lq_set_fio)
    lq_set_fio = list(lq_set_fio)
    for row in lq_list:
        fio = row[3]
        for lq_fio in lq_set_fio:
            if lq_fio == fio:
                lq_set_by_names.append(tuple(row))
    lq_set_by_names = set(lq_set_by_names)
    lq_list_2 = set(lq_list_2)
    lq_set_by_names.difference_update(lq_list_2)
    output_list = list(lq_set_by_names)
    return output_list


def converted_cmd_list_to_usual(cmd_list_after_compare, cmd_list_before_convertion,
                                lq_remaining_list):
    output_list = [['','',''],['Повторяющиеся услуги в счете CMD:', '', ''],
                   ['№ заявки', 'ФИО', 'Код услуги CMD']]
    non_unique = [row for row in cmd_list_before_convertion if cmd_list_before_convertion.count(row) > 1]
    non_unique = set(non_unique)
    non_unique = list(non_unique)
    for row in non_unique:
        output_list.append(row[0:3])
    output_list.append(['', '', ''])
    output_list.append(['Не были найдены в выгрузке LQ:', '', ''])
    for row in cmd_list_after_compare:
        output_list.append(row[0:3])
    output_list.append(['', '', ''])
    output_list.append(['Тезки с другими услугами:', '', ''])
    for row in lq_remaining_list:
        output_list.append(row[2:5])
    return output_list


def compare(cmd_input_excel_file, lq_input_csv_file, compare_result_file):
    csv1 = tempfile.NamedTemporaryFile(delete=False)
    csv2 = tempfile.NamedTemporaryFile(delete=False)
    data_xlsx = pd.read_excel(cmd_input_excel_file, sheet_name=0)
    data_xlsx.to_csv(str(csv1.name), encoding='utf-8', sep=';', index=False, header=False)
    cmd_bill_list = cmd_bill_convert(cmd_list=(csv_to_list(str(csv1.name), encoding='utf-8')))
    lq_bill_list = lq_bill_convert(lq_list=(csv_to_list(lq_input_csv_file, encoding='utf-8')))
    compare_result = bill_compare(cmd_list=cmd_bill_list, lq_list=lq_bill_list)
    lq_remains = lq_remaining(cmd_list=cmd_bill_list, lq_list=lq_bill_list)
    result_list = converted_cmd_list_to_usual(cmd_list_after_compare=compare_result,
                                              cmd_list_before_convertion=cmd_bill_list,
                                              lq_remaining_list=lq_remains)
    list_to_csv(result_list, str(csv2.name))
    data_csv = pd.read_csv(str(csv2.name), delimiter=';', encoding='utf-8',
                           index_col=None, quoting=csv.QUOTE_MINIMAL, quotechar='|', )
    data_csv.to_excel(compare_result_file, encoding='cp1251', index=False, merge_cells=False, header=False)
    csv1.close()
    csv2.close()
    os.unlink(csv1.name)
    os.unlink(csv2.name)


# def compare2(cmd_input_excel_file='LQ18.xls', lq_input_csv_file='LQ.csv', compare_result_file=None):
#     csv1 = tempfile.NamedTemporaryFile(delete=False)
#     csv2 = tempfile.NamedTemporaryFile(delete=False)
#     data_xlsx = pd.read_excel(cmd_input_excel_file, sheet_name=0)
#     data_xlsx.to_csv(str(csv1.name), encoding='utf-8', sep=';', index=False, header=False)
#     cmd_bill_list = cmd_bill_convert(cmd_list=(csv_to_list(str(csv1.name), encoding='utf-8')))
#     print(cmd_bill_list)
#     print('\n'*5)
#     lq_bill_list = lq_bill_convert(lq_list=(csv_to_list(lq_input_csv_file, encoding='utf-8')))
#     print(lq_bill_list)
#     compare_result = bill_compare(cmd_list=cmd_bill_list, lq_list=lq_bill_list)
#     #print(compare_result)
#     # lq_remains = lq_remaining(cmd_list=cmd_bill_list, lq_list=lq_bill_list)
#     # result_list = converted_cmd_list_to_usual(cmd_list_after_compare=compare_result,
#     #                                           cmd_list_before_convertion=cmd_bill_list,
#     #                                           lq_remaining_list=lq_remains)
#     # list_to_csv(result_list, str(csv2.name))
#     # data_csv = pd.read_csv(str(csv2.name), delimiter=';', encoding='utf-8',
#     #                        index_col=None, quoting=csv.QUOTE_MINIMAL, quotechar='|', )
#     # data_csv.to_excel(compare_result_file, encoding='cp1251', index=False, merge_cells=False, header=False)
#     csv1.close()
#     csv2.close()
#     os.unlink(csv1.name)
#     os.unlink(csv2.name)
#
# compare2()