# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(QtWidgets.QWidget):

    def setupUi(self, MainWindow):
        MainWindow.setWindowTitle('Сравнение счетов')
        MainWindow.setGeometry(QtCore.QRect(800, 400, 360, 200))
        MainWindow.setWindowIcon(QtGui.QIcon('billcompare/icon.png'))

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.cmdLabel = QtWidgets.QLabel()
        self.cmdLabel.setText('Счет CMD')
        self.lqLabel = QtWidgets.QLabel()
        self.lqLabel.setText('Выгрузка LQ (csv)')
        self.outputLabel = QtWidgets.QLabel()
        self.outputLabel.setText('Результат сверки')

        self.cmdFilePath = QtWidgets.QLineEdit()
        self.lqFilePath = QtWidgets.QLineEdit()
        self.resultFilePath = QtWidgets.QLineEdit()

        self.cmdOpen = QtWidgets.QPushButton()
        self.cmdOpen.setText('Открыть')
        self.lqOpen = QtWidgets.QPushButton()
        self.lqOpen.setText('Открыть')
        self.resultSave = QtWidgets.QPushButton()
        self.resultSave.setText('Сохранить как')
        self.compareButton = QtWidgets.QPushButton()
        self.compareButton.setText('Сравнить')
        self.closeButton = QtWidgets.QPushButton()
        self.closeButton.setText('Закрыть')

        self.hbox = QtWidgets.QHBoxLayout()
        self.hbox.addWidget(self.compareButton)
        self.hbox.addStretch(1)
        self.hbox.addWidget(self.closeButton)

        self.grid = QtWidgets.QGridLayout()
        self.grid.addWidget(self.cmdLabel, 0, 0)
        self.grid.addWidget(self.cmdOpen, 1, 0)
        self.grid.addWidget(self.lqLabel, 2, 0)
        self.grid.addWidget(self.lqOpen, 3, 0)
        self.grid.addWidget(self.outputLabel, 4, 0)
        self.grid.addWidget(self.resultSave, 5, 0)

        self.grid.addWidget(self.cmdFilePath, 1, 1)
        self.grid.addWidget(self.lqFilePath, 3, 1)
        self.grid.addWidget(self.resultFilePath, 5, 1)

        self.grid.addLayout(self.hbox, 6, 0, 6, 2)

        self.centralwidget.setLayout(self.grid)

        MainWindow.setCentralWidget(self.centralwidget)



class Ui_DialogOK(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setGeometry(800, 400, 272, 79)
        self.messageText = QtWidgets.QLabel(Dialog)
        self.messageText.setObjectName("label")
        self.pushButtonOK = QtWidgets.QPushButton(Dialog)
        self.pushButtonOK.setFixedSize(86, 28)
        self.pushButtonOK.setObjectName("pushButtonOK")

        Dialog.hbox = QtWidgets.QHBoxLayout()
        Dialog.hbox.addStretch(1)
        Dialog.hbox.addWidget(self.pushButtonOK)

        Dialog.vbox = QtWidgets.QVBoxLayout()
        Dialog.vbox.addWidget(self.messageText)
        Dialog.vbox.addStretch(1)
        Dialog.vbox.addLayout(self.hbox)

        Dialog.setLayout(Dialog.vbox)


        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle("Ошибка")
        self.messageText.setText("Что-то пошло не так")
        self.pushButtonOK.setText("OK")


class One_button_dialog(QtWidgets.QDialog, Ui_DialogOK):
    def __init__(self):
        QtWidgets.QDialog.__init__(self)
        self.setupUi(self)
        self.pushButtonOK.clicked.connect(self.close)