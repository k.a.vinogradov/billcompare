#!/usr/bin/env python

from billcompare.main import MWin
from PyQt5.QtWidgets import QApplication
import sys

app = QApplication(sys.argv)
pw = MWin()
pw.show()
sys.exit(app.exec())